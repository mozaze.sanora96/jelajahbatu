<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/terkini', 'BeritaTerkiniController@index');
Route::get('/sudutkota', 'SudutController@index');
Route::get('/inspiratif', 'InspiratifController@index');
Route::get('/mampir', 'MampirController@index');
Route::get('/suara', 'SuaraController@index');
Route::get('/tentang', 'TentangController@index');

Route::get('/admindashboard', 'AdminDashboardController@index');
Route::get('/adminartikel', 'AdminArtikelController@index');
Route::get('/createartikel', 'CreateArtikelController@index');
Route::get('/adminimages', 'AdminImagesController@index');
Route::get('/createimages', 'CreateImagesController@index');
Route::get('/adminusers', 'UserController@index');