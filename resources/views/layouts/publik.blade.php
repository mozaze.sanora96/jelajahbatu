<!DOCTYPE html>
<html>
<head>
<title img src="images/llogo fix.png" alt="" />JELAJAH BATU </title>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Custom Theme files -->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- for bootstrap working -->
    <script type="text/javascript" src="home/js/bootstrap.js"></script>
<!-- //for bootstrap working -->
<!-- web-fonts -->
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
<script src="js/responsiveslides.min.js"></script>
    <script>
        $(function () {
          $("#slider").responsiveSlides({
            auto: true,
            nav: true,
            speed: 500,
            namespace: "callbacks",
            pager: true,
          });
        });
    </script>
    <script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<!--/script-->
<script type="text/javascript">
            jQuery(document).ready(function($) {
                $(".scroll").click(function(event){     
                    event.preventDefault();
                    $('html,body').animate({scrollTop:$(this.hash).offset().top},900);
                });
            });
</script>
</head>
<body>
    <!-- header-section-starts-here -->
    <div class="header">
        <div class="header-top">
            <div class="wrap">
                <div class="top-menu">
                    <ul>
                        <li><a href="#">Contact Us !</a></li>
                    </ul>
                </div>
                <div class="num">
                    <p> Call us : xxxx xxxx xxxx </p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="header-bottom">
            <div class="logo text-center">
                <a href="/"><img src="images/logo jelajah batu fix.png" alt="" /></a>
            </div>
            <div class="navigation">
                <nav class="navbar navbar-default" role="navigation">
           <div class="wrap">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                
            </div>
            <!--/.navbar-header-->
        
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                     <li class="active"><a href="/">Home</a></li>
                        <li><a href="/terkini">Terkini</a></li>
                        <li><a href="/sudutkota">Sudut Kota</a></li>
                        <li><a href="/inspiratif">Inspiratif</a></li>
                        <li><a href="/mampir">Mampir Yuks!</a></li>
                        <li><a href="/suara">Suara Kita</a></li>
                        <li><a href="/tentang">Tentang Kami</a></li>
                      
                        </li>
                    <div class="clearfix"></div>
                </ul>
                <div class="search">
                    <!-- start search-->
                    <div class="search-box">
                        <div id="sb-search" class="sb-search">
                            <form>
                                <input class="sb-search-input" placeholder="Mau Cari ... " type="search" name="search" id="search">
                                <input class="sb-search-submit" type="submit" value="">
                                <span class="sb-icon-search"> </span>
                            </form>
                        </div>
                    </div>
                    <!-- search-scripts -->
                    <script src="js/classie.js"></script>
                    <script src="js/uisearch.js"></script>
                        <script>
                            new UISearch( document.getElementById( 'sb-search' ) );
                        </script>
                    <!-- //search-scripts -->
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!--/.navbar-collapse-->
     <!--/.navbar-->
            </div>
        </nav>
        </div>
    </div>
    <!-- header-section-ends-here -->
    <div class="wrap">
        <div class="move-text">
            <div class="breaking_news">
                <h2>Breaking News</h2>
            </div>
            <div class="marquee">
                <div class="marquee1"><a class="breaking" href="single.html">>>Judul Berita Terbaru</a></div>
                <div class="marquee2"><a class="breaking" href="single.html">>>Judul Berita Terbaru</a></div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
            <script type="text/javascript" src="js/jquery.marquee.min.js"></script>
            <script>
              $('.marquee').marquee({ pauseOnHover: true });
              //@ sourceURL=pen.js
            </script>
        </div>
    </div>
    <!-- content-section-starts-here -->

    @yield('content')
            <div class="col-md-4 side-bar">
            <div class="first_half">
                <div class="list_vertical">

                     </div>
                     
                        
                     </div>
                     </div>
                     <div class="secound_half">              
                     <div class="popular-news">
                    </div>
                    </div>
                    <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="footer">
        <div class="footer-top">
            <div class="wrap">
                <div class="col-md-2 col-xs-6 col-sm-2 footer-grid">
                    <h4 class="footer-head">Kategori</h4>
                    <ul class="cat">
                        <li><a href="/">Suara Kita</a></li>
                        <li><a href="/">Inspiratif</a></li>
                        <li><a href="/">Terkini</a></li>
                        <li><a href="/">Sudut Kota</a></li>
                        <li><a href="/">Mampir Yuks</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-xs-12 footer-grid">
                    <h4 class="footer-head">Contact Us</h4>
                    <span class="hq">Jelajah Batu</span>
                    <address>
                        <ul class="location">
                            <li><span class="glyphicon glyphicon-map-marker"></span></li>
                            <li>Jl. Raya Tlogomas Gg. 15c No. 07, Kec. Lowokwaru, Malang, Jawa Timur</li>
                            <div class="clearfix"></div>
                        </ul>   
                        <ul class="location">
                            <li><span class="glyphicon glyphicon-earphone"></span></li>
                            <li>xxxx xxxx xxxx</li>
                            <div class="clearfix"></div>
                        </ul>   
                        <ul class="location">
                            <li><span class="glyphicon glyphicon-envelope"></span></li>
                            <li><a href="mailto:info@example.com">mail@example.com</a></li>
                            <div class="clearfix"></div>
                        </ul>                       
                    </address>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="wrap">
                <div class="copyrights col-md-6">
                    <p> © Core ID. All Rights Reserved | Design by  <a href="http://w3layouts.com/"> CoreID</a></p>
                </div>
                <div class="footer-social-icons col-md-6">
                    <ul>
                        <li><a class="facebook" href="#"></a></li>
                        <li><a class="twitter" href="#"></a></li>
                        <li><a class="flickr" href="#"></a></li>
                        <li><a class="googleplus" href="#"></a></li>
                        <li><a class="dribbble" href="#"></a></li>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
                /*
                var defaults = {
                wrapID: 'toTop', // fading element id
                wrapHoverID: 'toTopHover', // fading element hover id
                scrollSpeed: 1200,
                easingType: 'linear' 
                };
                */
        $().UItoTop({ easingType: 'easeOutQuart' });
});
</script>
<a href="#to-top" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!---->
</body>
</html>