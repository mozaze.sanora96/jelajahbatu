@extends('layouts.admin')

@section('content')

<hr>
<button type="button" class="btn btn-secondary">Tambah Artikel</button>
<hr>
<table class="table table-dark">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Judul</th>
      <th scope="col">Penulis</th>
      <th scope="col" width="230">Teks</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>Mark</td>
      <td>Otto</td>
      <td></td>
      <td>
        <button class="btn button btn-Success">Edit</button>
        <button class="btn btn-danger">Delete</button>
      </td>
    </tr>
  </tbody>
</table>
<!-- <table class="table table-bordered table-striped table-dark">
  <thead>
    <tr>
      <th scope="col" >No</th>
      <th scope="col" colspan="2">Judul</th>
      <th scope="col" rowspan="2">Teks</th>
      <th scope="col" rowspan="2">Penulis</th>
      <th scope="col" rowspan="2">Action</th>
    </tr>
  </thead>
  <tbody>

  	</tbody> -->

@endsection