<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Jelajah Batu | Authentication</title>
        <link href="css/style (2).css" rel="stylesheet" type="text/css" />
        <link href="css/fontawesome-all (2).css" rel="stylesheet" />
        <link href="//fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    
        

    </head>

    <body>

        @yield('content')


    </body >

</html>