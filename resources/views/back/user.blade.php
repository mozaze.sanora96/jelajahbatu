@extends('layouts.admin')

@section('content')

<hr>
<a class="btn button btn-primary" href="{{ route('register') }} ">Register</a>
<hr>
<table class="table table-dark">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama</th>
      <th scope="col">Email</th>
      <th scope="col">Password</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>Mark</td>
      <td>Otto</td>
      <td></td>
      <td>
        <a class="btn button btn-warning" href="{{ route('password.request') }} ">Reset</a>
        <button class="btn btn-danger">Delete</button>
      </td>
    </tr>
  </tbody>
</table>
<!-- <table class="table table-bordered table-striped table-dark">
  <thead>
    <tr>
      <th scope="col" >No</th>
      <th scope="col" colspan="2">Judul</th>
      <th scope="col" rowspan="2">Teks</th>
      <th scope="col" rowspan="2">Penulis</th>
      <th scope="col" rowspan="2">Action</th>
    </tr>
  </thead>
  <tbody>

  	</tbody> -->

@endsection