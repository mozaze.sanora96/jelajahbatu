@extends('layouts.form')


@section('content')
		
<div class="header">
	<h1>Form  Gambar</h1>
</div>
	<div class="main-content">
		<div class="contact-w3">

			<form action="#" method="post">
				<label>Nama Gambar</label>
				<input type="text" name="judul" placeholder="Nama Gambar" required>
				<div class="row">
					<div class="contact-left-w3">
						<label for="file" class="col-form-label">Files</label>
      					<input type="file" class="form-control"  name="file" value="">
					</div>
					<div class="clear"></div>
				</div>
				<input type="submit" value="Kirim Artikel">
			</form>
		</div>
	</div>
@endsection