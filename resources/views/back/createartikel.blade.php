@extends('layouts.form')


@section('content')
		
<div class="header">
	<h1>Form Artikel</h1>
</div>
	<div class="main-content">
		<div class="contact-w3">

			<form action="#" method="post">
				<label>Judul</label>
				<input type="text" name="judul" placeholder="Judul Artikel" required>
				<div class="row">
					<div class="contact-left-w3">
						<label>Penulis</label>
							<input type="text" name="penulis" placeholder="Nama Penulis" required>
					</div>
					<div class="clear"></div>
				</div>
				<div class="row1">
					<label>Teks</label>
					<textarea placeholder="Teks" name="teks"></textarea>
				</div>
				<input type="submit" value="Kirim Artikel">
			</form>
		</div>
	</div>


@endsection