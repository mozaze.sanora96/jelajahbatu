@extends('layouts.publik')

@section('content')

<div class="main-body">
	<div class="wrap">
		<div class="col-md-8 content-left single-post">
			<div class="blog-posts">
				<h3 class="post">Penjual Es campur berumur 80 tahun, Viral</h3>
				<div class="last-article">
					<p class="artext">Siapa menyangka kekek penjulan Es campur  legendaris  di Kota Batu ini adalah pengusaha sukses yang sesungguhnya. meski sudah kerap diberitakan di media lokal dan media sosial, namun belum banyak yang tahu tentang kehidupan pribadinya. Sejak 1954 berjualan Es Campur hingga sekarang, ternyata sudah banyak kesuksesan yang ia miliki. Walau harga Es campur yang ia jual tergolong sangat murah, namun berkat kerja keras yang ia lakukan, ia berhasil mewujudkan cita-cita yang diimpikanya selama ini, salah satunyamempunyai rumah dan kendaraan yang bagus serta bisa pergi untuk beribadah Umroh.</p>
					<p class="artext"> Tidak ada yang tidak mungkin didunia ini asal kita berusah, itulah yang terjadi pada Mbah said, begitu panggilan dekatnya. selama 64 tahun berjualan Es campur  ternyata bukanlah garisan hidup yang buruk baginya. Berkat  kesabaran, keikhlasan dan kerja keras yang ia lakukan selama ini akhirnya berbuah dengan manis. Dulunya hanya mempunyai rumah dari bambu yang berukuran sangat kecil, namun akhirnya saat ini sudah memiliki rumah yang cukup besar dan nyaman, selain itu juga dari hasil berjualan Es campur ia berhasil  menyekohkan kedua anaknya keperguruan tinggi hingga saat ini anak dan cucunya sudah menjadi pengusaha dan pekerja yang sukses.</p>
					<p class="artext">Tidak terhitung jatuh bangun yang ia alami sampai pada titik ini, namun semua itu ia nikmati dan syukuri sehingga tidak pernah sekalipun berniat untuk berganti profesi. Sejak ia berumur 16 tahun, Berjualan Es campur menjadi pilihan satu-satunya untuk mencari penghidupan. Tidak menyangka bisa sekuses ini, dulua ia hanya bertujuan untuk menyambung hidup dan menafkahi keluarga kecilnya. ......... menjadi perempuan yang paling beruntung, karena ketegaran Mbah Said da</p>
					<p class="artext">Mbah Said  juga Sempat dibicarakan di berbagai media termasuk media sosial, seperti Instagram.  Bukan karena hanya kenikmatan Es campurnya yang membuat ketagihan tapi juga karakter Mbah said yang sangat berkesan bagi pelanggan. Bagimana tidak, meski sudah berumur 80 tahun ia tetap kuat berjualan sampai malam ditambah lagi kecekatannya melayani pemebeli yang melebihi dari sebayanya memang semangat yang sangat luar biasa.</p>

					<p class="artext">Meski sudah sering ditegur anak-anaknya untuk tidak perlu lagi berjualan, Mbah Said tidak menghiraukannya, “Satu hari saya nggak jualan badan saya sakit. Meskipun sudah tua bukan berarti berhenti beraktivitas, karena otak dan pikiran tetap harus bekerja.” Ungkapnya saat ditemui dikediamannya di Jl. </p>
					<p class="artext">“Bapak itu pulang malam, makan juga sering lupa, tapi alhamdullilah masih sehat dan masih bisa bantu semua orang lewat jualan.” Tamabah ..... Istri Mbah Said.  Meski sudah menuai hasil, Mbah Said tidak pernah berubah, tetap sederhana, pekerja keras,  dan menjadikan pekerjaanya sebagai ladang amal. Hasil berjualan Es yang tidak banyak tetap bisa dibagi-bagi untuk sesama dan hebatnya lagi masih bisa ditabung untuk membeli segala yang sudah dimiliki seperti sekarang ini. Inilah pembuktian konsistensi dan kesetiannya terhadap profesi. “ saya harus mencari teman hidup terlebih dahulu, sebelum mencari pasangan hidup. Meski hanya berjualan Es campur apalagi dulu hanya keliling, saya tetap yakin dan sabar. Akhirnya memang benar, berjualan Es campur adalah teman hidup saya yang paling awet sampai saat ini.” pungkasnya.
					<p class="artext"> Kesuksesan membutuhkan proses dan waktu yang panjang, tekin dan bekerja keras menjadi dasar serta keikhlasan dan kesabaran menjadi kunci utamanya. Tidak ada yang tidak mungkin apabila kita ingin berusaha. Ditengah banyaknya profesi yang dianggap lebih baik tapi kesabaran dan keikhlasan lah menjadi kunci sukses Mbah Said hingga sampai pada kesuksesan yang ak pernah ia bayangkan sebelumnya. Ini membuktikan keberhasilan bisa datang dari pekerjaan apa saja dan dari potensi diri yang mana saja, semuanya tergantung dari diri kita masing-masing untuk bisa memanfaatkannya dengan sebaik mungkin atau tidak.</p>
					<div class="clearfix"></div>
			</div>
			<div class="response">
			<h4>Responses</h4>
			<div class="media response-info">
				<div class="media-left response-text-left">
					<a href="#">
						<img class="media-object" src="images/c1.jpg" alt=""/>
					</a>
					<h5><a href="#">Username</a></h5>
				</div>
				<div class="media-body response-text-right">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,There are many variations of passages of Lorem Ipsum available, 
						sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					<ul>
						<li>Sep 21, 2015</li>
						<li><a href="single.html">Reply</a></li>
					</ul>
				</div>
				<div class="clearfix"> </div>
			</div>
				<div class="coment-form">
					<h4>Leave your comment</h4>
					<form>
						<input type="text" value="Name " onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Name';}" required="">
						<input type="email" value="Email (will not be published)*" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email (will not be published)*';}" required="">
						<input type="text" value="Website" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Website';}" required="">
						<textarea onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Your Comment...';}" required="">Your Comment...</textarea>
						<input type="submit" value="Submit Comment" >
					</form>
				</div>	
		</div>    
	</div>
</div>


@endsection