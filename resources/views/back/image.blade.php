@extends('layouts.admin')
@section('content')
<hr>
<button type="button" class="btn btn-secondary">Tambah Gambar</button>
<hr>
<table class="table table-dark">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama Gambar</th>
      <th scope="col">Gambar</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>Mark</td>
      <td>Otto</td>
      <td>
        <button class="btn button btn-Success">Edit</button>
        <button class="btn btn-danger">Delete</button>
      </td>
    </tr>
  </tbody>
</table>


@endsection